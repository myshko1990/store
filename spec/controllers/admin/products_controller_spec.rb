require 'rails_helper'

RSpec.describe Admin::ProductsController, type: :controller do
  before :all do
    @products = FactoryGirl.create(:product)
  end
  
  context 'user is signed in' do
    before :each do
      @request.env["devise.mapping"] = Devise.mappings[:product]
      sign_in :product, @product
    end
    describe 'GET #index' do
      before :each do
        get :index
      end
    end
  end
end
