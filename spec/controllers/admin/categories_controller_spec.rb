require 'rails_helper'

RSpec.describe Admin::CategoriesController, type: :controller do
  before :all do
    @user = FactoryGirl.create(:user)
    @category = FactoryGirl.create(:category)
    
    # category1 = FactoryGirl.create(:category, user: @user, published: true)
    # category2 = FactoryGirl.create(:category, user: @user, published: false)
    # category3 = FactoryGirl.create(:category, user: @user, published: true)
    # @categories = [category1, category2, category3]
  end

  context 'user is signed in' do
    before :each do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in :user, @user
    end
    
    describe 'GET #index' do
      before :each do
        get :index
      end
      it 'should have successful response status' do
        expect(response).to be_success
      end
      it 'should have valid content-type' do
        expect(response.content_type).to eq('text/html')
      end
      it 'should render #index view' do
        expect(response).to render_template(:index)
      end
    end

    describe 'GET #show' do
      before :each do
        get :show, id: @category
      end
      it 'should have successful response status' do
        expect(response).to be_success
      end
      it 'should have valid content-type' do
        expect(response.content_type).to eq('text/html')
      end
      it 'should render #show view' do
        expect(response).to render_template(:show)
      end
    end
    describe 'GET #new' do
      before :each do
        get :new
      end
      it 'should have successful response status' do
        expect(response).to be_success
      end
      it 'should have valid content-type' do
        expect(response.content_type).to eq('text/html')
      end
      it 'should render #new view' do
        expect(response).to render_template(:new)
      end
    end

    describe 'GET #edit' do
      before :each do
#        category = @categories[0]
        get :edit, id: @category
      end
      it 'should have successful response status' do
        expect(response).to be_success
      end
      it 'should have valid content-type' do
        expect(response.content_type).to eq('text/html')
      end
      it 'should render #edit view' do
        expect(response).to render_template(:edit)
      end
    end

    describe 'POST #create' do
      context 'with valid article params' do
        before :each do
          category_params = FactoryGirl.attributes_for(:category, user: nil)
          post :create, category: category_params
          @category = Category.find_by(title: category_params[:title])
        end
      end
      it 'should create new article' do
        expect(@category).not_to eq(nil)
      end
      # it 'should assign current user as author' do
      #   expect(@category.user).to eq(@user)
      # end
      it 'should redirect to #show article' do
        redirect_to(admin_category_path(@category))
      end
    end
  end
end