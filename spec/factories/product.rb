FactoryGirl.define do
  factory :product do
    # + 1 word for title
    title { Faker::Lorem.word }
    description { Faker::Lorem.word }
    # + number
    sku { Faker::Number.number(10) }
    price { Faker::Number.decimal(3, 2) }
    # for picture
    picture { Faker::Avatar.image }
  end
end