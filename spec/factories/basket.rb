FactoryGirl.define do
  factory :basket do
    total_price { Faker::Number.decimal(3, 2) }
  end
end