FactoryGirl.define do
  factory :busket_item do
    quantity { Faker::Number.decimal(3) }
  end
end