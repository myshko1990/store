FactoryGirl.define do
  factory :category do
    # + 1 words for title in category
    title { Faker::Lorem.words }
  end
end