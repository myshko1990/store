require 'rails_helper'

RSpec.describe Basket, type: :model do
  it 'have a valid factory' do
    expect(FactoryGirl.create(:basket)).to be_valid
  end
  
  context 'relations' do
    it { should have_and_belong_to_many(:products) }
    # it { should have_one(:busket_item) }
    # it { should belong_to(:busket_item) }
  end
  
  context 'validations' do
    context 'fields' do
      it { should validate_presence_of(:total_price) }
    end
  end
end