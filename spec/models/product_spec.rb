require 'rails_helper'

RSpec.describe Product, type: :model do
  it 'have a valid factory' do
    expect(FactoryGirl.create(:product)).to be_valid
  end
  
  context 'relations' do
    it { should belong_to(:category) }
    # it { should have_one(:busket_item) }
    it { should have_and_belong_to_many(:baskets) }
    # it { should belong_to(:busket_item) }
  end
  
  context 'validations' do
    context 'fields' do
      it { should validate_presence_of(:title) }
      it { should validate_presence_of(:sku) }
#      it { should validate_uniqueness_of(:sku) }
#      it { should validate_presence_of(:price) }
    end
  end
end