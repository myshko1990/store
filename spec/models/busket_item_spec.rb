require 'rails_helper'

RSpec.describe BusketItem, type: :model do
  it 'have a valid factory' do
    expect(FactoryGirl.create(:busket_item)).to be_valid
  end
  
  context 'relations' do
    # it { should have_one(:product) }
    # it { should have_many(:baskets) }
    it { should belong_to(:product) }
    it { should belong_to(:basket) }
  end

  context 'validations' do
    context 'fields' do
      it { should validate_presence_of(:quantity) }
    end
  end
end