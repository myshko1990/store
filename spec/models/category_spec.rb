require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'have a valid factory' do
    expect(FactoryGirl.create(:category)).to be_valid
  end
  
  context 'relations' do
    it { should have_many(:products) }
  end
  
  context 'validations' do
    it { should validate_presence_of(:title) }
  end
end