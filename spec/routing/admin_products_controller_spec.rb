require 'rails_helper'

RSpec.describe 'Routes for Admin::ProductsController', type: :routing do
  context 'RESTful CRUD' do
    it 'routes GET "/admin/products" to "admin/products#index"' do
      expect(get('/admin/products')).to route_to('admin/products#index')
    end
    it 'routes GET "/admin/products/1" to "admin/products#show"' do
      expect(get('/admin/products/1')).to route_to('admin/products#show', id: '1')
    end
    it 'routes GET "/admin/products/new" to "admins/products#new"' do
      expect(get('/admin/products/new')).to route_to('admin/products#new')
    end
    it 'routes GET "/admin/products/1/edit" to "admin/products#edit"' do
      expect(get('/admin/products/1/edit')).to route_to('admin/products#edit', id: '1')
    end
    it 'routes POST "/admin/products" to "admin/products#create"' do
      expect(post('/admin/products')).to route_to('admin/products#create')
    end
    it 'routes PUT/PATCH "/admin/products/1" to "admin/products#update"' do
      expect(put('/admin/products/1')).to route_to('admin/products#update', id: '1')
      expect(patch('/admin/products/1')).to route_to('admin/products#update', id: '1')
    end
    it 'routes DELETE"/admin/products/1" to "admin/products#destroy"' do
      expect(delete('/admin/products/1')).to route_to('admin/products#destroy', id: '1')
    end
  end
end