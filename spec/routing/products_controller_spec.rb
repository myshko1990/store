require 'rails_helper'

RSpec.describe 'Routes for ProductsController', type: :routing do
  context 'Show end Index for users' do
    it 'routes GET "/admin/products" to "admin/products#index"' do
      expect(get('/admin/products')).to route_to('admin/products#index')
    end
    it 'routes GET "/admin/products/1" to "admin/products#show"' do
      expect(get('/admin/products/1')).to route_to('admin/products#show', id: '1')
    end
  end
end