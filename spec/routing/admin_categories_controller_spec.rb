require 'rails_helper'

RSpec.describe 'Routes for Admin::CategoryController', type: :routing do
  context 'RESTful CRUD' do
    it 'routes GET "/admin/categories" to "admin/categories#index"' do
      expect(get('/admin/categories')).to route_to('admin/categories#index')
    end
    it 'routes GET "/admin/categories/1" to "admin/categories#show"' do
      expect(get('/admin/categories/1')).to route_to('admin/categories#show', id: '1')
    end
    it 'routes GET "/admin/categories/new" to "admins/categories#new"' do
      expect(get('/admin/categories/new')).to route_to('admin/categories#new')
    end
    it 'routes GET "/admin/categories/1/edit" to "admin/categories#edit"' do
      expect(get('/admin/categories/1/edit')).to route_to('admin/categories#edit', id: '1')
    end
    it 'routes POST "/admin/categories" to "admin/categories#create"' do
      expect(post('/admin/categories')).to route_to('admin/categories#create')
    end
    it 'routes PUT/PATCH "/admin/categories/1" to "admin/categories#update"' do
      expect(put('/admin/categories/1')).to route_to('admin/categories#update', id: '1')
      expect(patch('/admin/categories/1')).to route_to('admin/categories#update', id: '1')
    end
    it 'routes DELETE"/admin/categories/1" to "admin/categories#destroy"' do
      expect(delete('/admin/categories/1')).to route_to('admin/categories#destroy', id: '1')
    end
  end
end