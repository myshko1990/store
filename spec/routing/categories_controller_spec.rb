require 'rails_helper'

RSpec.describe 'Routes for CategoryController', type: :routing do
  context 'Show end Index for users' do
    it 'routes GET "/admin/categories" to "admin/categories#index"' do
      expect(get('/admin/categories')).to route_to('admin/categories#index')
    end
    it 'routes GET "/admin/categories/1" to "admin/categories#show"' do
      expect(get('/admin/categories/1')).to route_to('admin/categories#show', id: '1')
    end
  end
end