class CreateBusketItems < ActiveRecord::Migration
  def change
    create_table :busket_items do |t|
      t.string :quantity
      t.integer :basket_id
      t.integer :product_id
      t.timestamps null: false
    end
    add_index :busket_items, :basket_id
    add_index :busket_items, :product_id
  end
end