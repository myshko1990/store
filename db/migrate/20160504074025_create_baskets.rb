class CreateBaskets < ActiveRecord::Migration
  def change
    create_table :baskets do |t|
      t.decimal :total_price, precision: 15, scale: 2

      t.timestamps null: false
    end
  end
end
