class BasketsProductsRelationship < ActiveRecord::Migration
  def change
    create_table :baskets_products, id: false do |t|
      t.integer :basket_id
      t.integer :product_id
    end
 
    add_index :baskets_products, :basket_id
    add_index :baskets_products, :product_id
  end
end
