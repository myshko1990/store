class CategoriesController < ApplicationController
  # GET /products
  def index
    @categories = Category.all
    @products = Product.all
  end
  
  # GET /products/1
  def show
    @category = Category.find(params[:id])
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:title, :product_id, :picture)
    end
end