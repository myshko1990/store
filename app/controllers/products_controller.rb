class ProductsController < ApplicationController
  # GET /products
  def index
    @products = Product.all
    @categories = Category.all
  end
  
  # GET /products/1
  def show
    @product = Product.find(params[:id])
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title, :sku, :description, :price, :category_id, :picture)
    end
end