class Admin::CategoriesController < Admin::AdminController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  
  # GET /products
  def index
    @categories = Category.all
  end
  
  # GET /products/1
  def show
  end
  
  # GET /products/new
  def new
    @category = Category.new
    @product = Product.new
  end
  
  # GET /products/1/edit
  def edit
    @products = Product.all
  end
  
  # POST /products
  def create
    @category = Category.new(category_params)
    
    if @category.save
      redirect_to admin_category_path(@category), notice: 'Category was successfully created.'
    else
      render :new
    end
  end
  
  # PATCH/PUT /products/1
  def update
    if @category.update(category_params)
      redirect_to admin_category_path(@category), notice: 'Category was successfully updated.'
    else
      render :edit
    end
  end
  
  # DELETE /products/1
  def destroy
    @category.destroy
    redirect_to admin_categories_path, notice: 'Category was successfully destroyed.'
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:title, :product_id, :picture)
    end
end