class BasketsController < ApplicationController
  def index
    @baskets = Basket.all
    @products = Product.all
  end
  
  # GET /products/1
  def show
    @basket = Basket.find(params[:id])
  end
  
  def edit
    @products = Product.all
  end
  
  # PATCH/PUT /products/1
  def update
    if @basket.update(basket_params)
      redirect_to basket_path(@basket), notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end
  
  # DELETE /products/1
  def destroy
    @basket.destroy
    redirect_to basket_path, notice: 'Product was successfully destroyed.'
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:basket).permit(:total_price, :product_id)
    end
end
