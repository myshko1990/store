class Product < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_and_belongs_to_many :baskets
  has_one :busket_item
  belongs_to :busket_item
  
  validates :title, presence: true
  validates :sku, presence: true, uniqueness: true
  validates :price, numericality: { greater_than: 0 }
  
  mount_uploader :picture, PictureUploader
  
end