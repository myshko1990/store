class User < ActiveRecord::Base
  
  # Declaration of relationships
  has_many :products, dependent: :destroy
  has_many :categories, dependent: :destroy
    
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end