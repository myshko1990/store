class Category < ActiveRecord::Base
  has_many :products
  belongs_to :user
  
  validates :title, presence: true
  
  mount_uploader :picture, PictureUploader
end