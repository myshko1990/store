class BusketItem < ActiveRecord::Base
  has_one :product
  has_many :baskets
  belongs_to :product
  belongs_to :basket
  
  validates :quantity, presence: true
end
