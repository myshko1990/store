class Basket < ActiveRecord::Base
  has_and_belongs_to_many :products
  has_one :busket_item
  belongs_to :busket_item
  
  validates :total_price, presence: true, numericality: { greater_than: 0 }
end
