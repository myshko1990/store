Rails.application.routes.draw do
  
  root 'categories#index'
  
  devise_for :users
  
  resources :products, only: [:index, :show]
  resources :categories, only: [:index, :show]
  resources :baskets
  
  namespace :admin do
    resources :products
    resources :categories
  end
end